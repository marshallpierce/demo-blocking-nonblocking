A playground to show some aspects of blocking vs nonblocking workloads.

To run a blocking workload of 100,000 tasks per second for 10,000,000 tasks total with 80ms delay per task:

```
./gradlew run --args "100000 10000000 80 blocking"
```

And for nonblocking:

```
./gradlew run --args "100000 10000000 80 blocking"
```
