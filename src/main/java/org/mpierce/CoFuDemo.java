package org.mpierce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CoFuDemo {

    private final Map<String, Tagger> taggers = Map.of(
            "alpha", new AlphaTagger(null),
            "bravo", new BravoTagger(null));

    private final List<String> defaultData = List.of();

    public static void main(String[] args) {
        var list = List.of(1, 2, 3, 4, 5);

        var result = list.stream()
                .reduce((left, right) -> {
                    System.out.println("left = " + left + ", right = " + right);
                    return left + right;
                });

        System.out.println("result = " + result);
    }

    void httpServer() {
        while (true) {
            // get request from http socket
            var request = new Request(List.of());

            handleRequest(request).thenApply(response -> writeHttpResponse(response));
        }
    }

    private Void writeHttpResponse(Object response) {
        // no op
        return null;
    }

    CompletableFuture<List<List<String>>> handleRequest(Request request) {
        var pendingResponses = taggers.values()
                .stream()
                .map(tagger -> tagger.tokenize(request.titles)
                        .handle((data, throwable) -> {
                            if (throwable != null) {
                                // TODO log error, etc
                                return defaultData;
                            } else {
                                return data;
                            }
                        })
                        .completeOnTimeout(defaultData, 80, TimeUnit.MILLISECONDS))
                .collect(Collectors.toList());

        return transpose(pendingResponses);
    }

    static <T> CompletableFuture<List<T>> transpose(List<CompletableFuture<T>> futures) {
        return futures
                // List<CoFu<Int>>
                .stream()
                // List<CoFu<List<Int>>
                .map(f -> f.thenApply(t -> {
                    // must be mutable
                    List<T> list = new ArrayList<>(1);
                    list.add(t);
                    return list;
                }))
                .reduce((CompletableFuture<List<T>> leftCf, CompletableFuture<List<T>> rightCf) -> leftCf
                        .thenCombine(rightCf, (List<T> left, List<T> right) -> {
                            left.addAll(right);
                            return left;
                        })).orElseGet(() -> CompletableFuture.completedFuture(List.of()));
    }

    class Request {
        final List<String> titles;

        Request(List<String> titles) {
            this.titles = titles;
        }
    }

    interface Tagger {
        CompletableFuture<List<String>> tokenize(List<String> titles);
    }

    interface TaggerAlphaClient {
        CompletableFuture<List<String>> tokenize(List<String> titles);
    }

    interface TaggerBravoClient {
        CompletableFuture<String[]> tokenize(String[] titles);
    }

    class AlphaTagger implements Tagger {
        private final TaggerAlphaClient client;

        AlphaTagger(TaggerAlphaClient client) {
            this.client = client;
        }

        @Override
        public CompletableFuture<List<String>> tokenize(List<String> titles) {
            return client.tokenize(titles);
        }
    }

    class BravoTagger implements Tagger {
        private final TaggerBravoClient client;

        BravoTagger(TaggerBravoClient client) {
            this.client = client;
        }

        @Override
        public CompletableFuture<List<String>> tokenize(List<String> titles) {
            return client.tokenize(titles.toArray(new String[]{})).thenApply(Arrays::asList);
        }
    }
}
