package org.mpierce

import org.HdrHistogram.Histogram
import org.jctools.queues.MessagePassingQueue
import org.jctools.queues.MpmcUnboundedXaddArrayQueue
import java.time.Duration
import java.time.Instant
import java.util.concurrent.Callable
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.math.max
import kotlin.system.exitProcess

object BlockingVsNonblocking {
    @JvmStatic
    fun main(args: Array<String>) {
        val ratePerSec = args[0].toInt()
        val total = args[1].toLong()
        val delay = args[2].toLong()
        val mode = args[3]

        // risky -- thread pool could grow without bound.
        // Could also use a fixed size pool with a queue, but backpressure is a problem there too.
        val pool = Executors.newCachedThreadPool()
        val client = StubClient(delay)

        val workload: (Long) -> Histogram = when (mode) {
            "blocking" -> {
                { iterations -> runBlocking(pool, client, ratePerSec, iterations) }
            }
            "nonblocking" -> {
                { iterations -> runNonblocking(client, ratePerSec, iterations) }
            }
            else -> {
                System.err.println("Unknown mode $mode")
                exitProcess(1)
            }
        }

        // warm up
        workload(10_000)

        println("warmup done")

        val start = Instant.now()
        // run for real
        val histogram = workload(total)

        val end = Instant.now()

        // scale nanosecond measurements to millis
        histogram.outputPercentileDistribution(System.out, 2,1_000_000.0)

        println("Duration: ${Duration.between(start, end)}")

        pool.shutdown()
        client.close()
    }
}

fun runBlocking(pool: ExecutorService, client: StubClient, ratePerSec: Int, total: Long): Histogram {
    return runPeriodicTask(ratePerSec, total) { queue, start ->
        pool.submit {
            // block
            client.getData().get()
            queue.spinOffer(System.nanoTime() - start)
        }
    }
}

fun runNonblocking(client: StubClient, ratePerSec: Int, total: Long): Histogram =
    runPeriodicTask(ratePerSec, total) { queue, start ->
        client.getData()
            .whenComplete { _, _ ->
                queue.spinOffer(System.nanoTime() - start)
            }
    }

fun runPeriodicTask(
    ratePerSec: Int,
    total: Long,
    doWorkAndEnqueueDuration: (MessagePassingQueue<Long>, Long) -> Unit
): Histogram {
    val sleepMillis = 1000L / ratePerSec
    val sleepNanosPerTask = 1_000_000_000 / ratePerSec
    // Thread.sleep() uses nanos only to go beyond millisecond precision
    val sleepNanosAfterMillis = sleepNanosPerTask - sleepMillis.toInt() * 1_000_000

    // use an MPSC queue to capture the duration measurements efficiently
    val queue: MessagePassingQueue<Long> = MpmcUnboundedXaddArrayQueue(100)
    val pool = Executors.newSingleThreadExecutor()
    val histFuture = pool.submit(HistogramAccumulator(queue))

    var remaining = total

    // keep track of task start times as when they _should_ have started if sleep timing was perfect
    // this is necessary to pass the "ctrl z test": if you pause the process, do you see correspondingly huge growth
    // in the upper percentile times?
    var sleepStart = System.nanoTime()
    while (remaining > 0) {
        Thread.sleep(sleepMillis, sleepNanosAfterMillis)
        val sleepEnd = System.nanoTime()
        val sleepDuration = sleepEnd - sleepStart

        // sleep granularity isn't fine enough for any reasonable benchmark, so we will need to start multiple per sleep
        val tasksToStart = max(1, sleepDuration / sleepNanosPerTask).toInt()

        var taskStart = sleepStart + sleepNanosPerTask
        for (i in 0..tasksToStart) {
            // give each task the start time it should have had if sleep worked perfectly
            doWorkAndEnqueueDuration(queue, taskStart)
            taskStart += sleepNanosPerTask
        }

        remaining -= tasksToStart
        sleepStart = sleepEnd
    }

    // signal HistogramAccumulator to stop
    queue.spinOffer(Long.MIN_VALUE)

    pool.shutdown()

    return histFuture.get()
}

class HistogramAccumulator(private val queue: MessagePassingQueue<Long>) : Callable<Histogram> {
    var done = false
    override fun call(): Histogram {
        val histogram = Histogram(3)
        while (true) {
            val drained = queue.drain { num ->
                // a somewhat approximate method of shutdown; we might miss a few records at the end but it doesn't
                // matter for our purposes here
                if (num == Long.MIN_VALUE) {
                    // don't record this value, but continue to record available data in this drain()
                    done = true
                } else {
                    histogram.recordValue(num)
                }
            }

            if (done) {
                return histogram
            }

            if (drained == 0) {
                Thread.sleep(1)
            }
        }
    }
}

fun <T> MessagePassingQueue<T>.spinOffer(element: T) {
    var success = false
    while (!success) {
        // spin until offer succeeds -- should happen rarely or never since consumer is fast
        success = offer(element)
    }
}

class StubClient(private val delayMs: Long) : AutoCloseable {
    private val completer = Executors.newScheduledThreadPool(2)

    fun getData(): CompletableFuture<String> {
        val future = CompletableFuture<String>()

        completer.schedule({ future.complete("hello") }, delayMs, TimeUnit.MILLISECONDS)

        return future
    }

    override fun close() {
        completer.shutdown()
    }
}
