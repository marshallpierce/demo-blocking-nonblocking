plugins {
    java
    application
    kotlin("jvm") version "1.4.21"
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.hdrhistogram:HdrHistogram:2.1.12")
    implementation("org.jctools:jctools-core:3.1.0")
}

application {
    mainClass.set("org.mpierce.BlockingVsNonblocking")
}
